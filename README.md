# Stoplight

## Using CLI

### Install Stoplight-recommended CLI

```bash
npm install -g @apidevtools/swagger-cli
```

### Validate

```bash
swagger-cli validate reference/openapi.yaml
```

### Combine Multiple Files

```bash
swagger-cli bundle --dereference --type yaml --outfile _build/openapi.yaml reference/openapi.yaml
```

## Useful Resources

### Tools

- [Swagger/OpenAPI CLI](https://github.com/APIDevTools/swagger-cli)

### Generate API Docs

- [Elements](https://github.com/stoplightio/elements/blob/main/docs/getting-started/elements/html.md)
- [Swagger UI](https://github.com/swagger-api/swagger-ui/blob/master/docs/usage/installation.md)

### Articles

- [Keeping OpenAPI DRY and Portable](https://blog.stoplight.io/keeping-openapi-dry-and-portable)
- [Splitting specification file](https://apihandyman.io/writing-openapi-swagger-specification-tutorial-part-8-splitting-specification-file/)
- [How to split a large OpenAPI document into multiple files](https://davidgarcia.dev/posts/how-to-split-open-api-spec-into-multiple-files/)
